HOW TO INSTALL:
First of all, grab a copy of CodeBlocks, open the project and build it (Release mode).
If you don't want to compile it manually you can always download the binary file from
my blog @ http://3mptylab.blogspot.it/2012/09/how-to-make-asus-notebooks-media-keys.html

- AUTOMATICALLY:
  Run install.bat as administrator (right click on it->Run as administrator).

- MANUALLY:
  Replace original DMedia.exe (C:\Program Files (x86)\ASUS\ATK Package\ATK Media) with the one provided.

CREDITS:
- Original code by http://zaak404.wordpress.com
- keybd_event code by http://http://3mptylab.blogspot.com/

License'n'stuff:
- Icon.ico
  AUTHOR: http://www.deleket.com/
  LICENSE: CC Attribution-Noncommercial-No Derivate 3.0
